﻿justunits
=========

.. automodule:: justunits

   
   
   .. rubric:: Module Attributes

   .. autosummary::
   
      PREFIX_FOR_ONE
      BareUnit
      DEFAULT_ATTRIBUTE_UNIT_SEPARATOR
      DEFAULT_ALLOWED_SEPARATORS
      DEFAULT_POWER_STYLE
      DEFAULT_DIVIDING_STYLE
      DEFAULT_SEPARATION_STYLE
      DEFAULT_UNIT_STYLE
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      create_unit
      from_string
      join_unit
      prefix_base_unit
      reformat_unit
      register_unit
      reset_unit_library
      set_default_attribute_unit_separator
      set_default_unit_style
      split_attribute_unit
      split_number_and_unit
      split_unit
      split_unit_text
      to_string
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      AUnit
      DerivedUnit
      AttributeUnitSeparators
      BaseSiUnits
      BareUnit
      Prefix
      SiPrefixes
      SiUnits
      StyledDetection
      UnitDividingStyle
      UnitPowerStyle
      UnitSeparationStyle
      UnitStyle
      UnknownUnit