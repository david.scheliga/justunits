***************************
justunits.AUnit
***************************

.. autosummary::
   :toctree:

   justunits.AUnit
   justunits.AUnit.switch_power
   justunits.AUnit.power
   justunits.AUnit.width
   justunits.AUnit.first_letter
   justunits.AUnit.symbol
   justunits.AUnit.name
   justunits.AUnit.quantity
   justunits.AUnit.base_symbol
   justunits.AUnit.prefix_symbol
   justunits.AUnit.with_power
   justunits.AUnit.with_prefix
   justunits.AUnit.without_prefix
   justunits.AUnit.bare
   justunits.AUnit.to_raw_parts
   justunits.AUnit.UNIT_TYPE