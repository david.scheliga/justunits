***************************
API reference
***************************

.. autosummary::
   :toctree:

   justunits
   justunits.DerivedUnit
   justunits.AttributeUnitSeparators
   justunits.BaseSiUnits
   justunits.BareUnit
   justunits.create_unit
   justunits.from_string
   justunits.join_unit
   justunits.Prefix
   justunits.prefix_base_unit
   justunits.reformat_unit
   justunits.register_unit
   justunits.reset_unit_library
   justunits.set_default_attribute_unit_separator
   justunits.set_default_unit_style
   justunits.SiPrefixes
   justunits.SiUnits
   justunits.split_unit
   justunits.split_unit_text
   justunits.StyledDetection
   justunits.to_string
   justunits.UnitDividingStyle
   justunits.UnitPowerStyle
   justunits.UnitSeparationStyle
   justunits.UnitStyle
   justunits.UnknownUnit


.. toctree::

   AUnit/index